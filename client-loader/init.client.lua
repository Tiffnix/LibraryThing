local ReplicatedStorage = game:GetService("ReplicatedStorage")

-- currentRoot will become a clone of source during the first reload
local source = ReplicatedStorage:WaitForChild("Modules")
local currentRoot = source
local mainModule = "Client"

script.Disabled = true

local Facade = {
	_watching = {},
	_beforeUnload = nil,
}

--[[
	Sets the method to call the next time the system tries to reload
]]
function Facade:beforeUnload(callback)
	self._beforeUnload = callback
end

function Facade:_load(savedState)
	local ok, result = pcall(require, currentRoot:FindFirstChild(mainModule))

	if not ok then
		warn("Failed to load: " .. result)
		return
	end

	local main = result
	coroutine.wrap(main)(Facade, savedState)
end

function Facade:_reload()
	local saveState
	if self._beforeUnload then
		saveState = self._beforeUnload()
		self._beforeUnload = nil
	end

	currentRoot = source:Clone()

	self:_load(saveState)
end

function Facade:_watch(instance)
	if self._watching[instance] then
		return
	end

	-- Don't watch ourselves!
	if instance == script then
		return
	end

	local connection1 = instance.Changed:Connect(function(prop)
		print("Reloading due to", instance:GetFullName())

		self:_reload()
	end)

	local connection2 = instance.ChildAdded:Connect(function(instance)
		self:_watch(instance)
	end)

	local connections = {connection1, connection2}

	self._watching[instance] = connections

	for _, child in ipairs(instance:GetChildren()) do
		self:_watch(child)
	end
end

Facade:_load()
Facade:_watch(source)
