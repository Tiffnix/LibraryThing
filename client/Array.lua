local Array = {}

function Array.insert(array, pos, value)
	if pos < 0 then
		pos = #array + 1 + pos
	end

	local newArray = {}
	local j = 1
	for i = 1, pos do
		newArray[j] = array[i]
		j = j + 1
	end
	newArray[j] = value
	j = j + 1
	for i = pos + 1, #array do
		newArray[j] = array[i]
		j = j + 1
	end

	return newArray
end

function Array.replace(array, pos, value)
	if pos < 0 then
		pos = #array + 1 + pos
	end

	local newArray = {}
	for i = 1, #array do
		newArray[i] = array[i]
	end
	newArray[pos] = value

	return newArray
end

function Array.replaceMap(array, pos, func)
	if pos < 0 then
		pos = #array + 1 + pos
	end

	local oldValue = array[pos]
	local newValue = func(oldValue)
	return Array.replace(array, pos, newValue)
end

function Array.remove(array, pos)
	if pos < 0 then
		pos = #array + 1 + pos
	end

	local newArray = {}
	local j = 1
	for i = 1, pos - 1 do
		newArray[j] = array[i]
		j = j + 1
	end
	for i = pos + 1, #array do
		newArray[j] = array[i]
		j = j + 1
	end
	return newArray
end

function Array.map(array, func)
	local newArray = {}
	for i = 1, #array do
		newArray[i] = func(array[i])
	end
	return newArray
end

return Array
