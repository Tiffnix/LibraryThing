local identPattern = '^([a-zA-Z_][a-zA-Z0-9_]*)$'

local function isValidIndex(key, length)
	return
		typeof(key) == 'number' and
		math.floor(key) == key and
		key >= 1 and
		key <= length
end

local function prettyTable(value, indent)
	indent = indent or ''
	local valueTy = typeof(value)
	if valueTy == 'table' then
		local output = {'{'}
		for key, child in pairs(value) do
			local childStr = prettyTable(child, indent .. '  ')
			if typeof(key) == 'string' and key:match(identPattern) then
				output[#output+1] = string.format("%s = %s,", key, childStr)
			elseif not isValidIndex(key, #value) then
				local keySafe = prettyTable(key, indent .. '  ')
				output[#output+1] = string.format("[%s] = %s,", keySafe, childStr)
			end
		end
		for i = 1, #value do
			local childStr = prettyTable(value[i], indent .. '  ')
			output[#output+1] = string.format("%s,", childStr)
		end
		local result = table.concat(output, '\n'..indent..'  ')
		result = result .. '\n' .. indent .. '}'
		return result
	elseif valueTy == 'string' then
		return string.format("%q", value)
	else
		return tostring(value)
	end
end

return prettyTable
