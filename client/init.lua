local Players = game:GetService("Players")

local Modules = script.Parent
local Roact = require(Modules.Roact)
local TestEZ = require(Modules.TestEZ)

local App = require(script.Components.App)
local TextEditor = require(script.TextEditor)

local function main(facade, saveState)
	local player = Players.LocalPlayer
	local playerGui = player:WaitForChild("PlayerGui")

	TestEZ.TestBootstrap:run(Modules.Client)

	local textEditor = saveState or TextEditor.newEmpty()
	local setTextEditor
	local handle
	function setTextEditor(newEditor)
		textEditor = newEditor
		handle = Roact.reconcile(handle, Roact.createElement(App, {
			textEditor = textEditor,
			setTextEditor = setTextEditor,
		}), "App")
	end

	handle = Roact.mount(Roact.createElement(App, {
		textEditor = textEditor,
		setTextEditor = setTextEditor,
	}), playerGui, "App")

	facade:beforeUnload(function()
		Roact.unmount(handle)
		return textEditor
	end)
end

return main
