local Node = {}
Node.__index = Node

Node.Type = {
	Document = 'Document',
	BlockQuote = 'BlockQuote',
	Paragraph = 'Paragraph',
	Heading1 = 'Heading1',
	Heading2 = 'Heading2',
	Heading3 = 'Heading3',
	Heading4 = 'Heading4',
	Heading5 = 'Heading5',
	Heading6 = 'Heading6',
	BlockCode = 'BlockCode',
	UnorderedList = 'UnorderedList',
	OrderedList = 'OrderedList',
	ListItem = 'ListItem',
}

function Node.new(type)
	local self = {
		type = Node.Type[type] or error("Invalid node type"),
		children = {},
		open = false,
	}
	setmetatable(self, Node)

	return self
end

local patterns = {
	h1 = "^%s*#%s*(.*)%s*$",
	h2 = "^%s*##%s*(.*)%s*$",
	h3 = "^%s*###%s*(.*)%s*$",
	h4 = "^%s*####%s*(.*)%s*$",
	h5 = "^%s*#####%s*(.*)%s*$",
	h6 = "^%s*######%s*(.*)%s*$",
	paragraph = "^%s*(.+)%s*$",
	blockquote = "^%s*>%s*(.*)%s*$",
	codeBlock = "^%s*```%s*$",
	emptyLine = "^%s*$",
	unorderedList = "^(%s*[%*%-%+]%s*)(.*)$",
	orderedList = "^(%s*[0-9]+%.%s*)(.*)$",
}

local blocks = {
	-- pattern, type, open
	{ patterns.h1, Node.Type.Heading1, false },
	{ patterns.h2, Node.Type.Heading2, false },
	{ patterns.h3, Node.Type.Heading3, false },
	{ patterns.h4, Node.Type.Heading4, false },
	{ patterns.h5, Node.Type.Heading5, false },
	{ patterns.h6, Node.Type.Heading6, false },
}

function Node:createChild(line)
	for _,entry in pairs(blocks) do
		local pattern = entry[1]
		local type = entry[2]
		local open = entry[3]
		local result = line:match(pattern)
		if result then
			local node = Node.new(type)
			node.text = result
			node.open = open == nil and true or open
			self.children[#self.children+1] = node
			return true
		end
	end
	local blockquote = line:match(patterns.blockquote)
	if blockquote then
		local node = Node.new(Node.Type.BlockQuote)
		node.open = true
		self.children[#self.children+1] = node
		node:createChild(blockquote)
		return true
	end
	if line:match(patterns.codeBlock) then
		local node = Node.new(Node.Type.BlockCode)
		node.open = true
		self.children[#self.children+1] = node
		return true
	end
	local listPrefix, list = line:match(patterns.unorderedList)
	if listPrefix and list then
		local node = Node.new(Node.Type.UnorderedList)
		node.open = true
		node.prefix = listPrefix
		self.children[#self.children+1] = node

		local item = Node.new(Node.Type.ListItem)
		item.open = true
		node.children[#node.children+1] = item
		item:createChild(list)

		return true
	end
	local listPrefix, list = line:match(patterns.orderedList)
	if listPrefix and list then
		local node = Node.new(Node.Type.OrderedList)
		node.open = true
		node.prefix = listPrefix
		self.children[#self.children+1] = node

		local item = Node.new(Node.Type.ListItem)
		item.open = true
		item.number = 1
		node.children[#node.children+1] = item
		item:createChild(list)

		return true
	end
	local paragraph = line:match(patterns.paragraph)
	if paragraph then
		local node = Node.new(Node.Type.Paragraph)
		node.text = paragraph
		node.open = true
		self.children[#self.children+1] = node

		return true
	end
end

-- returns whether or not it will eat the line, which determines whether or not it's open or closed
function Node:feedLine(line)
	if self.type == Node.Type.Document then
		local child = self.children[#self.children]
		if child and child.open then
			local result = child:feedLine(line)
			if result then
				return true
			end
		end

		return self:createChild(line)
	elseif self.type == Node.Type.BlockQuote then
		local quoted = line:match(patterns.blockquote)
		if quoted then
			local child = self.children[#self.children]
			if child and child.open then
				local result = child:feedLine(quoted)
				if result then
					return true
				end
			end

			self:createChild(quoted)
			return true
		elseif line:match(patterns.emptyLine) then
			return true
		else
			self.open = false
			return false
		end
	elseif self.type == Node.Type.Paragraph then
		line = line:match(patterns.paragraph)
		if line then
			self.text = self.text .. ' ' .. line
			return true
		end
		self.open = false
		return false
	elseif self.type == Node.Type.BlockCode then
		if line:match(patterns.codeBlock) then
			self.open = false
			return true
		end
		if self.text then
			self.text = self.text .. '\n' .. line
		else
			self.text = line
		end
		return true
	elseif self.type == Node.Type.OrderedList then
		local wsPrefix = self.prefix:gsub('[0-9%.]', ' ')
		local _, listed = line:match(patterns.orderedList)
		if line:sub(1, #wsPrefix) == wsPrefix then
			local rest = line:sub(#wsPrefix+1, -1)

			local child = self.children[#self.children]
			if child and child.open then
				local result = child:feedLine(rest)
				if result then
					return true
				end
			end

			return self:createChild(rest)
		elseif listed then
			local node = Node.new(Node.Type.ListItem)
			node.open = true
			node.number = #self.children + 1
			self.children[#self.children+1] = node
			node:createChild(listed)
			return true
		end
	elseif self.type == Node.Type.UnorderedList then
		local wsPrefix = self.prefix:gsub('[%*%-]', ' ')
		if line:sub(1, #wsPrefix) == wsPrefix then
			local rest = line:sub(#wsPrefix+1, -1)

			local child = self.children[#self.children]
			if child and child.open then
				local result = child:feedLine(rest)
				if result then
					return true
				end
			end

			return self:createChild(rest)
		elseif line:sub(1, #self.prefix) == self.prefix then
			local rest = line:sub(#self.prefix+1, -1)

			local node = Node.new(Node.Type.ListItem)
			node.open = true
			self.children[#self.children+1] = node
			node:createChild(rest)
			return true
		end
	elseif self.type == Node.Type.ListItem then
		local child = self.children[#self.children]
		if child and child.open then
			local result = child:feedLine(line)
			if result then
				return true
			end
		end

		return self:createChild(line)
	end
	return false
end

local Markdown = {}
Markdown.__index = Markdown
Markdown.Node = Node

function Markdown.new()
	local self = {
		document = Node.new(Node.Type.Document),
	}
	setmetatable(self, Markdown)

	return self
end

function Markdown:getDocument()
	return self.document
end

function Markdown:feedString(input)
	if input:sub(-1) ~= '\n' then
		input = input .. '\n'
	end
	input = input:gsub('\r', '')

	for line in input:gmatch("(.-)\n") do
		self:feedLine(line)
	end
end

function Markdown:feedLine(input)
	self.document:feedLine(input)
end

return Markdown
