local Modules = script.Parent.Parent.Parent
local Roact = require(Modules.Roact)
local Markdown = require(Modules.Client.Markdown)

local TextLabel = require(script.Parent.TextLabel)
local InlineTextLabel = require(script.Parent.InlineTextLabel)
local FitChildren = require(script.Parent.FitChildren)

local function MarkdownNode(props)
	local node = props.node

	if node.type == Markdown.Node.Type.Paragraph then
		return Roact.createElement(TextLabel, {
			LayoutOrder = props.LayoutOrder,
			Text = node.text,
			TextSize = 28,
			Font = Enum.Font.SourceSansLight,
		})
	elseif node.type == Markdown.Node.Type.Heading1 then
		return Roact.createElement(TextLabel, {
			LayoutOrder = props.LayoutOrder,
			Text = node.text,
			TextSize = 40,
			Font = Enum.Font.SourceSansLight,
		})
	elseif node.type == Markdown.Node.Type.BlockCode then
		return Roact.createElement(FitChildren.Frame, {
			LayoutOrder = props.LayoutOrder,
			BackgroundColor3 = Color3.fromRGB(228, 228, 232),
			BackgroundTransparency = 0.0,
			BorderSizePixel = 0,
			Padding = 4,
			SizeFit = FitChildren.FitAxis.Height,
			Size = UDim2.new(1, 0, 0, 0),
		}, {
			Code = Roact.createElement(TextLabel, {
				Text = node.text,
				TextSize = 18,
				Font = Enum.Font.Code,
			}),
		})
	elseif node.type == Markdown.Node.Type.BlockQuote then
		local children = {}
		for i, child in pairs(node.children) do
			children['Node'..i..child.type] = Roact.createElement(MarkdownNode, {
				LayoutOrder = i,
				node = child,
			})
		end
		return Roact.createElement(FitChildren.Frame, {
			LayoutOrder = props.LayoutOrder,
			Layout = {
				FillDirection = Enum.FillDirection.Horizontal,
				Padding = UDim.new(0, 4),
			},
			SizeFit = FitChildren.FitAxis.Height,
			BackgroundTransparency = 1.0,
			Size = UDim2.new(1, 0, 0, 0),
		}, {
			Divider = Roact.createElement("Frame", {
				LayoutOrder = 1,
				Size = UDim2.new(0, 3, 1, 0),
				BorderSizePixel = 0,
				BackgroundColor3 = Color3.fromRGB(150, 150, 150),
			}),
			Content = Roact.createElement(FitChildren.Frame, {
				LayoutOrder = 2,
				SizeFit = FitChildren.FitAxis.Height,
				Size = UDim2.new(1, 0, 0, 0),
				BackgroundTransparency = 1.0,
				Padding = 4,
			}, children)
		})
	elseif node.type == Markdown.Node.Type.ListItem then
		local children = {}
		for i, child in pairs(node.children) do
			children['Node'..i..child.type] = Roact.createElement(MarkdownNode, {
				LayoutOrder = i,
				node = child,
			})
		end
		local text = '• '
		if node.number then
			text = string.format("%d. ", node.number)
		end
		return Roact.createElement(FitChildren.Frame, {
			LayoutOrder = props.LayoutOrder,
			Layout = {
				FillDirection = Enum.FillDirection.Horizontal,
			},
			SizeFit = FitChildren.FitAxis.Height,
			Size = UDim2.new(1, 0, 0, 0),
			BackgroundTransparency = 1.0,
		}, {
			Bullet = Roact.createElement(InlineTextLabel, {
				LayoutOrder = 1,
				Text = text,
				TextSize = 28,
				Font = Enum.Font.SourceSansLight,
			}),
			Item = Roact.createElement(FitChildren.Frame, {
				LayoutOrder = 2,
				Size = UDim2.new(1, 0, 0, 0),
				SizeFit = FitChildren.FitAxis.Height,
				BackgroundTransparency = 1.0,
			}, children)
		})
	elseif node.type == Markdown.Node.Type.UnorderedList then
		local children = {}
		for i, child in pairs(node.children) do
			children['Node'..i..child.type] = Roact.createElement(MarkdownNode, {
				LayoutOrder = i,
				node = child,
			})
		end
		return Roact.createElement(FitChildren.Frame, {
			LayoutOrder = props.LayoutOrder,
			Layout = {
				Padding = UDim.new(0, 5),
			},
			SizeFit = FitChildren.FitAxis.Height,
			BackgroundTransparency = 1.0,
			Size = UDim2.new(1, 0, 0, 0),
		}, children)
	elseif node.type == Markdown.Node.Type.OrderedList then
		local children = {}
		for i, child in pairs(node.children) do
			children['Node'..i..child.type] = Roact.createElement(MarkdownNode, {
				LayoutOrder = i,
				node = child,
			})
		end
		return Roact.createElement(FitChildren.Frame, {
			LayoutOrder = props.LayoutOrder,
			Layout = {
				Padding = UDim.new(0, 5),
			},
			SizeFit = FitChildren.FitAxis.Height,
			BackgroundTransparency = 1.0,
			Size = UDim2.new(1, 0, 0, 0),
		}, children)
	else
		return Roact.createElement(TextLabel, {
			LayoutOrder = props.LayoutOrder,
			Text = string.format("<Unknown node type %q>", node.type),
		})
	end
end

return MarkdownNode
