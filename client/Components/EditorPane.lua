local TextService = game:GetService("TextService")
local RunService = game:GetService("RunService")
local UserInputService = game:GetService("UserInputService")

local Modules = script.Parent.Parent.Parent
local Roact = require(Modules.Roact)

local FitChildren = require(script.Parent.FitChildren)

local function EditorLine(props)
	local textSize = 18
	local font = Enum.Font.Code
	local gutterSize = 28
	local text = props.text

	local textWidth = TextService:GetTextSize('a', textSize, Enum.Font.Code, Vector2.new(10000, 10000)).X

	local function updateSize(rbx)
		if rbx then
			local frame = Vector2.new(rbx.AbsoluteSize.X - gutterSize - 2, 10000)
			local bounds = TextService:GetTextSize(text, textSize, font, frame)

			rbx.Size = UDim2.new(1, -gutterSize - 2, 0, bounds.Y)
		end
	end

	return Roact.createElement(FitChildren.Frame, {
		LayoutOrder = props.lineNumber,
		Size = UDim2.new(1, 0, 0, 0),
		SizeFit = FitChildren.FitAxis.Height,
		Layout = {
			FillDirection = Enum.FillDirection.Horizontal,
			Padding = UDim.new(0, 4),
		},
		BackgroundTransparency = 1.0,
	}, {
		LineNumber = Roact.createElement("TextLabel", {
			LayoutOrder = 1,
			Position = UDim2.new(0, 1, 0, 0),
			Size = UDim2.new(0, gutterSize, 0, textSize),
			TextXAlignment = Enum.TextXAlignment.Right,
			BackgroundTransparency = 1.0,
			Text = tostring(props.lineNumber),
			Font = font,
			TextSize = textSize,
			TextColor3 = Color3.fromRGB(100, 100, 100),
		}),
		TextBox = Roact.createElement("TextButton", {
			LayoutOrder = 2,
			BackgroundTransparency = 1.0,
			TextXAlignment = Enum.TextXAlignment.Left,
			TextYAlignment = Enum.TextYAlignment.Top,
			TextSize = textSize,
			Font = font,
			Text = text,
			TextColor3 = Color3.fromRGB(0, 0, 0),
			TextWrapped = true,

			[Roact.Ref] = updateSize,
			[Roact.Change.AbsoluteSize] = updateSize,
		}, {
			Cursor = props.cursor and Roact.createElement("Frame", {
				Visible = props.cursorVisible,
				Size = UDim2.new(0, 2, 0, textSize),
				Position = UDim2.new(
					0, props.cursor * textWidth,
					0, 0 * textSize),
				BackgroundColor3 = Color3.fromRGB(0, 0, 0),
				BorderSizePixel = 0,
			}),
		}),
	})
end

local EditorPane = Roact.PureComponent:extend("EditorPane")

function EditorPane:init()
	self.state = {
		focused = false,
	}
	self.timeCursorShown = 0.0
end

function EditorPane:keyPressed(input, method)
	self.props.setTextEditor(method(self.props.textEditor))
	self:resetCursor()
	local running = true
	local conn
	conn = UserInputService.InputEnded:Connect(function(otherInput)
		if input == otherInput then
			running = false
			conn:Disconnect()
		end
	end)
	delay(0.5, function()
		while running do
			self.props.setTextEditor(method(self.props.textEditor))
			self:resetCursor()
			wait(0.03)
		end
	end)
end

function EditorPane:didMount()
	self.steppedConn = RunService.RenderStepped:Connect(function(dt)
		self:stepped(dt)
	end)
	self.inputBeganConn = UserInputService.InputBegan:Connect(function(input)
		if input.KeyCode == Enum.KeyCode.Left then
			self:keyPressed(input, self.props.textEditor.arrowLeft)
		elseif input.KeyCode == Enum.KeyCode.Right then
			self:keyPressed(input, self.props.textEditor.arrowRight)
		elseif input.KeyCode == Enum.KeyCode.Up then
			self:keyPressed(input, self.props.textEditor.arrowUp)
		elseif input.KeyCode == Enum.KeyCode.Down then
			self:keyPressed(input, self.props.textEditor.arrowDown)
		elseif input.KeyCode == Enum.KeyCode.Backspace then
			self:keyPressed(input, self.props.textEditor.pressBackspace)
		end
	end)
end

function EditorPane:willUnmount()
	self.steppedConn:Disconnect()
	self.inputBeganConn:Disconnect()
end

function EditorPane:stepped(dt)
	if self.timeCursorHidden then
		self.timeCursorHidden = self.timeCursorHidden + dt
		if self.timeCursorHidden > 0.5 then
			self:resetCursor()
		end
	else
		self.timeCursorShown = self.timeCursorShown + dt
		if self.timeCursorShown > 0.5 then
			self.timeCursorHidden = 0.0
		end
	end
	self:setState({
		cursorVisible = self.state.focused and self.timeCursorHidden == nil,
	})
end

function EditorPane:resetCursor()
	self.timeCursorHidden = nil
	self.timeCursorShown = 0.0
end

function EditorPane:render()
	local props = self.props
	local textEditor = props.textEditor
	local cursor = textEditor.cursor
	local textSize = 18
	local gutterSize = 28

	local numLines = #textEditor.lines

	local lines = {}
	for i, line in pairs(textEditor.lines) do
		lines['Line'..i] = Roact.createElement(EditorLine, {
			lineNumber = i,
			text = line.text,
			cursor = cursor.lineNo == i and cursor.column,
			cursorVisible = self.state.cursorVisible,
		})
	end

	lines.UIListLayout = Roact.createElement("UIListLayout", {
		SortOrder = Enum.SortOrder.LayoutOrder,
	})

	return Roact.createElement("Frame", {
		LayoutOrder = props.LayoutOrder,
		Size = props.Size,
		BackgroundColor3 = Color3.fromRGB(238, 238, 238),
		BorderColor3 = Color3.fromRGB(160, 160, 160),
	}, {
		ScrollingFrame = Roact.createElement("ScrollingFrame", {
			BackgroundTransparency = 1.0,
			BorderSizePixel = 0,
			Size = UDim2.new(1, 0, 1, 0),
			CanvasSize = UDim2.new(0, 0, 0, numLines * textSize),
			TopImage = "rbxasset://textures/blackBkg_square.png",
			MidImage = "rbxasset://textures/blackBkg_square.png",
			BottomImage = "rbxasset://textures/blackBkg_square.png",
			VerticalScrollBarInset = Enum.ScrollBarInset.Always,
		}, {
			Gutter = Roact.createElement("Frame", {
				Size = UDim2.new(0, gutterSize, 1, 0),
				BackgroundColor3 = Color3.fromRGB(238, 238, 238),
				BorderSizePixel = 1,
				BorderColor3 = Color3.fromRGB(160, 160, 160),
			}),
			Background = Roact.createElement("Frame", {
				Size = UDim2.new(1, -gutterSize, 1, 0),
				BackgroundColor3 = Color3.fromRGB(255, 255, 255),
				BorderSizePixel = 0,
				Position = UDim2.new(0, gutterSize, 0, 0),
			}),
			TextBox = Roact.createElement("TextBox", {
				Size = UDim2.new(1, 0, 1, 0),
				BackgroundTransparency = 1.0,
				ZIndex = 3,
				TextTransparency = 1.0,

				[Roact.Change.Text] = function(rbx)
					if rbx.Text == '\r' or rbx.Text == '\n' then
						rbx.Text = ''
					elseif rbx.Text ~= '' then
						-- typed a character
						local char = rbx.Text
						props.setTextEditor(textEditor:typeChar(char))
						self:resetCursor()
						rbx.Text = ''
					end
				end,

				[Roact.Event.Focused] = function(rbx)
					self:setState({
						focused = true,
					})
					self:resetCursor()
				end,

				[Roact.Event.FocusLost] = function(rbx, enterPressed)
					if enterPressed then
						props.setTextEditor(textEditor:pressEnter())
						rbx:CaptureFocus()
						self:resetCursor()
					else
						self:setState({
							focused = false,
						})
					end
				end,
			}),
			Lines = Roact.createElement("Frame", {
				Size = UDim2.new(1, 0, 1, 0),
				BackgroundTransparency = 1.0,
				ZIndex = 2,
			}, lines)
		}),
	})
end

return EditorPane
