local Modules = script.Parent.Parent.Parent
local Roact = require(Modules.Roact)
local Markdown = require(Modules.Client.Markdown)
local prettyTable = require(Modules.Client.prettyTable)

local MarkdownNode = require(script.Parent.MarkdownNode)
local FitChildren = require(script.Parent.FitChildren)

local function Book(props)
	local text = props.text or ""

	local markdown = Markdown.new()
	markdown:feedString(text)
	local document = markdown:getDocument()

	local children = {}
	--[[children.UIListLayout = Roact.createElement("UIListLayout", {
		SortOrder = Enum.SortOrder.LayoutOrder,
		Padding = UDim.new(0, 10),
	})
	children.UIPadding = Roact.createElement("UIPadding", {
		PaddingLeft = UDim.new(0, 10),
		PaddingRight = UDim.new(0, 10),
		PaddingTop = UDim.new(0, 10),
		PaddingBottom = UDim.new(0, 10),
	})]]
	for i,node in pairs(document.children) do
		children['Node'..i..node.type] = Roact.createElement(MarkdownNode, {
			LayoutOrder = i,
			node = node,
		})
	end
	children.Debug = Roact.createElement(MarkdownNode, {
		LayoutOrder = 999999,
		node = {
			type = Markdown.Node.Type.BlockCode,
			text = prettyTable(document),
		}
	})

	return Roact.createElement(FitChildren.ScrollingFrame, {
		Padding = 10,
		Layout = {
			Padding = UDim.new(0, 10),
		},
		Size = props.Size,
		SizeFit = FitChildren.FitAxis.None,
		CanvasSizeFit = FitChildren.FitAxis.Height,
		Position = props.Position,
		BackgroundColor3 = Color3.fromRGB(245, 245, 238),
		VerticalScrollBarInset = Enum.ScrollBarInset.Always,
	}, children)
end

return Book
