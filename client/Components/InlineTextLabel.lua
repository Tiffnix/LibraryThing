local TextService = game:GetService("TextService")

local Modules = script.Parent.Parent.Parent
local Roact = require(Modules.Roact)

local function InlineTextLabel(props)
	local text = props.Text or "<props.Text>"
	local font = props.Font or Enum.Font.SourceSans
	local textSize = props.TextSize or 20

	local function updateSize(rbx)
		if rbx then
			local frame = Vector2.new(10000, 10000)
			local bounds = TextService:GetTextSize(text, textSize, font, frame)
			rbx.Size = UDim2.new(0, bounds.X, 0, bounds.Y)
		end
	end

	return Roact.createElement("TextLabel", {
		Position = props.Position,
		LayoutOrder = props.LayoutOrder,

		Text = text,
		Font = font,
		TextSize = textSize,
		TextWrapped = true,
		TextColor3 = props.TextColor3 or Color3.fromRGB(0, 0, 0),
		TextXAlignment = Enum.TextXAlignment.Left,
		TextYAlignment = Enum.TextYAlignment.Top,

		BackgroundTransparency = props.BackgroundTransparency or 1.0,
		BackgroundColor3 = props.BackgroundColor3,
		BorderSizePixel = props.BorderSizePixel,

		[Roact.Ref] = updateSize,
		[Roact.Change.AbsoluteSize] = updateSize,
	})
end

return InlineTextLabel
