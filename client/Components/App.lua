local Modules = script.Parent.Parent.Parent
local Roact = require(Modules.Roact)

local BookEditor = require(script.Parent.BookEditor)

local function App(props)
	return Roact.createElement("ScreenGui", {
		ZIndexBehavior = Enum.ZIndexBehavior.Sibling,
	}, {
		BookEditor = Roact.createElement(BookEditor, {
			textEditor = props.textEditor,
			setTextEditor = props.setTextEditor,
		}),
	})
end

return App
