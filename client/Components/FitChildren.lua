local Modules = script.Parent.Parent.parent
local Roact = require(Modules.Roact)

local FitAxis = {
	Width = 'Width',
	Height = 'Height',
	Both = 'Both',
	None = 'None',
}

local function resolvePadding(value)
	if typeof(value) == 'number' then
		return {
			left = value,
			right = value,
			top = value,
			bottom = value,
		}
	elseif typeof(value) == 'Vector2' then
		return {
			left = value.X,
			right = value.X,
			top = value.Y,
			bottom = value.Y,
		}
	elseif typeof(value) == 'table' then
		return value
	elseif value == nil then
		return {
			left = 0,
			right = 0,
			top = 0,
			bottom = 0,
		}
	end
end

local function FitChildrenFactory(name)
	local function FitChildren(props)
		local newProps = {}
		for key, value in pairs(props) do
			newProps[key] = value
		end
		newProps.Size = nil
		newProps.CanvasSize = nil
		newProps.SizeFit = nil
		newProps.CanvasSizeFit = nil
		newProps.Layout = nil
		newProps.Padding = nil
		newProps[Roact.Children] = nil

		local padding = resolvePadding(props.Padding)

		local function applySize(axis, axisBasis, bounds, orig)
			if axis == axisBasis or axis == FitAxis.Both then
				return UDim.new(0, bounds)
			end
			return orig
		end

		local function updateSize(rbx)
			if rbx then
				local pad = Vector2.new(
					padding.left + padding.right,
					padding.top + padding.bottom
				)
				local bounds = rbx.AbsoluteContentSize + pad

				if props.SizeFit then
					local orig = props.Size or UDim2.new(0, 0, 0, 0)
					rbx.Parent.Size = UDim2.new(
						applySize(props.SizeFit, FitAxis.Width, bounds.X, orig.X),
						applySize(props.SizeFit, FitAxis.Height, bounds.Y, orig.Y)
					)
				end
				if props.CanvasSizeFit then
					local orig = props.CanvasSize or UDim2.new(0, 0, 0, 0)
					rbx.Parent.CanvasSize = UDim2.new(
						applySize(props.CanvasSizeFit, FitAxis.Width, bounds.X, orig.X),
						applySize(props.CanvasSizeFit, FitAxis.Height, bounds.Y, orig.Y)
					)
				end
			end
		end

		local layoutProps = {
			SortOrder = Enum.SortOrder.LayoutOrder,
		}
		for key, value in pairs(props.Layout or {}) do
			layoutProps[key] = value
		end
		layoutProps[Roact.Ref] = updateSize
		layoutProps[Roact.Change.AbsoluteContentSize] = updateSize

		local newChildren = {}
		for key, value in pairs(props[Roact.Children]) do
			newChildren[key] = value
		end
		newChildren.Layout = Roact.createElement("UIListLayout", layoutProps)
		if props.Padding then
			newChildren.Padding = Roact.createElement("UIPadding", {
				PaddingLeft = UDim.new(0, padding.left),
				PaddingRight = UDim.new(0, padding.right),
				PaddingTop = UDim.new(0, padding.top),
				PaddingBottom = UDim.new(0, padding.bottom),
			})
		end

		return Roact.createElement(name, newProps, newChildren)
	end

	return FitChildren
end

local FitChildren = {}
FitChildren.FitAxis = FitAxis
FitChildren.Frame = FitChildrenFactory("Frame")
FitChildren.ScrollingFrame = FitChildrenFactory("ScrollingFrame")
return FitChildren
