local Modules = script.Parent.Parent.Parent
local Roact = require(Modules.Roact)

local EditorPane = require(script.Parent.EditorPane)
local Book = require(script.Parent.Book)

local BookEditor = Roact.PureComponent:extend("BookEditor")

function BookEditor:render()
	local textEditor = self.props.textEditor

	return Roact.createElement("Frame", {
		Size = UDim2.new(1, -20, 1, -20),
		Position = UDim2.new(0.5, 0, 0.5, 0),
		AnchorPoint = Vector2.new(0.5, 0.5),
		BackgroundTransparency = 1.0,
	}, {
		UIAspectRatioConstraint = Roact.createElement("UIAspectRatioConstraint", {
			AspectRatio = 1.5,
		}),
		EditorPane = Roact.createElement(EditorPane, {
			Size = UDim2.new(0.3, 0, 1, 0),
			textEditor = textEditor,
			setTextEditor = function(textEditor)
				self.props.setTextEditor(textEditor)
			end,
		}),
		Book = Roact.createElement(Book, {
			Size = UDim2.new(0.7, -20, 1, 0),
			Position = UDim2.new(0.3, 20, 0, 0),
			text = textEditor:getText(),
		})
	})
end

return BookEditor
