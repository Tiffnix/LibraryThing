local Cursor = {}
Cursor.__index = Cursor

function Cursor.new(lineNo, column)
	local self = {
		lineNo = lineNo,
		column = column,
	}
	setmetatable(self, Cursor)

	return self
end

function Cursor.__eq(left, right)
	return
		left.lineNo == right.lineNo and
		left.column == right.column
end

function Cursor:__tostring()
	return string.format("Cursor[%d:%d]", self.lineNo, self.column)
end

return Cursor
