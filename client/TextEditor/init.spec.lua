return function()
	local TextEditor = require(script.Parent)
	local Cursor = require(script.Parent.Cursor)
	local Line = require(script.Parent.Line)

	local exampleText = {
		Line.new("the quick brown fox"),
		Line.new("jumps over the lazy dog."),
	}

	describe("text editor", function()
		it("should not allow arrowing before the start of the file", function()
			local textEditor = TextEditor.new(exampleText, Cursor.new(1, 0))
			expect(textEditor:arrowLeft().cursor).to.equal(textEditor.cursor)
			expect(textEditor:arrowRight():arrowLeft():arrowLeft().cursor).to.equal(textEditor.cursor)
		end)

		it("should not allow arrowing past the end of the file", function()
			local textEditor = TextEditor.new(
				exampleText,
				Cursor.new(2, #("jumps over the lazy dog."))
			)
			expect(textEditor:arrowRight().cursor).to.equal(textEditor.cursor)
			expect(textEditor:arrowRight():arrowLeft():arrowRight().cursor).to.equal(textEditor.cursor)
		end)

		it("should allow arrowing right", function()
			local textEditor = TextEditor.new(
				exampleText,
				Cursor.new(1, 0)
			)
			expect(textEditor:arrowRight():arrowRight().cursor).to.equal(
				Cursor.new(1, 2)
			)
		end)

		it("should allow arrowing left", function()
			local textEditor = TextEditor.new(
				exampleText,
				Cursor.new(1, 2)
			)
			expect(textEditor:arrowLeft():arrowLeft().cursor).to.equal(
				Cursor.new(1, 0)
			)
		end)

		it("should allow typing chars", function()
			local textEditor = TextEditor.new(
				exampleText,
				Cursor.new(1, 0)
			)
			local result = textEditor:typeChar("f")
			expect(result.lines[1].text).to.equal("fthe quick brown fox")
			expect(result.cursor).to.equal(Cursor.new(1, 1))
		end)

		it("should allow pressing enter", function()
			local textEditor = TextEditor.new(
				exampleText,
				Cursor.new(1, #("the quick brown fox") + 1)
			)
			local result = textEditor:pressEnter()
			expect(#result.lines).to.equal(3)
			expect(result.lines[1].text).to.equal("the quick brown fox")
			expect(result.lines[2].text).to.equal("")
			expect(result.lines[3].text).to.equal("jumps over the lazy dog.")
			expect(result.cursor).to.equal(Cursor.new(2, 0))
		end)

		it("should allow enter to be pressed mid-line", function()
			local textEditor = TextEditor.new(
				exampleText,
				Cursor.new(1, #("the quick br"))
			)
			local result = textEditor:pressEnter()
			expect(#result.lines).to.equal(3)
			expect(result.lines[1].text).to.equal("the quick br")
			expect(result.lines[2].text).to.equal("own fox")
			expect(result.lines[3].text).to.equal("jumps over the lazy dog.")
			expect(result.cursor).to.equal(Cursor.new(2, 0))
		end)

		it("should allow hitting enter on empty lines", function()
			local textEditor = TextEditor.new(
				{
					Line.new(""),
					Line.new("friend")
				},
				Cursor.new(1, 0)
			)
			local result = textEditor:pressEnter()
			expect(#result.lines).to.equal(3)
			expect(result.lines[1].text).to.equal("")
			expect(result.lines[2].text).to.equal("")
			expect(result.lines[3].text).to.equal("friend")
			expect(result.cursor).to.equal(Cursor.new(2, 0))
		end)

		it("should support backspace", function()
			local textEditor = TextEditor.new(
				exampleText,
				Cursor.new(1, 2)
			)
			local result = textEditor:pressBackspace()
			expect(result.lines[1].text).to.equal("te quick brown fox")
		end)

		it("should allow getting the text", function()
			local textEditor = TextEditor.new(
				exampleText,
				Cursor.new(1, 0)
			)
			expect(textEditor:getText()).to.equal(
				"the quick brown fox\n"..
				"jumps over the lazy dog."
			)
		end)

	end)
end
