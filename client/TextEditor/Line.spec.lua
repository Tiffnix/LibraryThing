return function()
	local Line = require(script.Parent.Line)

	describe("line", function()
		it("should allow insertions at position 0", function()
			local line = Line.new("quick brown fox"):insert("the ", 0)
			expect(line.text).to.equal("the quick brown fox")
		end)

		it("should allow insertions at position -1", function()
			local line = Line.new("the quick brown "):insert("fox", -1)
			expect(line.text).to.equal("the quick brown fox")
		end)

		it("should allow deletions", function()
			local line = Line.new("the"):delete(2, 2)
			expect(line.text).to.equal("t")
		end)

		it("should allow deleting one char", function()
			local line = Line.new("the"):delete(2, 1)
			expect(line.text).to.equal("te")
		end)
	end)
end
