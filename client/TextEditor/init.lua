local Modules = script.Parent.Parent
local Array = require(Modules.Client.Array)

local Cursor = require(script.Cursor)
local Line = require(script.Line)

local TextEditor = {}
TextEditor.__index = TextEditor

function TextEditor.new(lines, cursor)
	local self = {
		lines = lines,
		cursor = cursor,
	}
	setmetatable(self, TextEditor)

	return self
end

function TextEditor.newEmpty()
	return TextEditor.new({ Line.new("") }, Cursor.new(1, 0))
end

function TextEditor.__eq(left, right)
	return
		left.lines == right.lines and
		left.cursor == right.cursor
end

function TextEditor:getText()
	local lines = Array.map(self.lines, function(line)
		return line.text
	end)
	return table.concat(lines, '\n')
end

function TextEditor:currentLine()
	return self.lines[self.cursor.lineNo]
end

function TextEditor:arrowRight()
	local line = self:currentLine()
	local len = #line.text
	if self.cursor.column >= len then
		if self.cursor.lineNo < #self.lines then
			local newCursor = Cursor.new(self.cursor.lineNo + 1, 0)

			return TextEditor.new(self.lines, newCursor)
		else
			return self
		end
	end

	local newCursor = Cursor.new(self.cursor.lineNo, self.cursor.column + 1)

	return TextEditor.new(self.lines, newCursor)
end

function TextEditor:arrowLeft()
	if self.cursor.column == 0 then
		if self.cursor.lineNo > 1 then
			local newCursor = Cursor.new(self.cursor.lineNo - 1, #self.lines[self.cursor.lineNo - 1].text)

			return TextEditor.new(self.lines, newCursor)
		else
			return self
		end
	end

	local newCursor = Cursor.new(self.cursor.lineNo, self.cursor.column - 1)

	return TextEditor.new(self.lines, newCursor)
end

function TextEditor:arrowUp()
	if self.cursor.lineNo > 1 then
		local column = math.min(
			self.cursor.column,
			#self.lines[self.cursor.lineNo - 1].text
		)
		local newCursor = Cursor.new(self.cursor.lineNo - 1, column)
		return TextEditor.new(self.lines, newCursor)
	else
		return self
	end
end

function TextEditor:arrowDown()
	if self.cursor.lineNo < #self.lines then
		local column = math.min(
			self.cursor.column,
			#self.lines[self.cursor.lineNo + 1].text
		)
		local newCursor = Cursor.new(self.cursor.lineNo + 1, column)
		return TextEditor.new(self.lines, newCursor)
	else
		return self
	end
end

function TextEditor:typeChar(char)
	local newCursor = Cursor.new(self.cursor.lineNo, self.cursor.column + 1)

	local newLines = Array.replaceMap(
		self.lines, self.cursor.lineNo,
		function(line)
			return line:insert(char, self.cursor.column)
		end
	)

	return TextEditor.new(newLines, newCursor)
end

function TextEditor:pressEnter()
	local newCursor = Cursor.new(self.cursor.lineNo + 1, 0)
	local line = self:currentLine()
	local beforeCursor = line.text:sub(1, self.cursor.column)
	local afterCursor = line.text:sub(self.cursor.column + 1, -1)
	-- inserts after
	local newLines = Array.replace(self.lines, self.cursor.lineNo, Line.new(beforeCursor))
	local newLines = Array.insert(newLines, self.cursor.lineNo, Line.new(afterCursor))

	return TextEditor.new(newLines, newCursor)
end

function TextEditor:pressBackspace()
	if self.cursor.column > 0 then
		local newCursor = Cursor.new(self.cursor.lineNo, self.cursor.column - 1)

		local newLines = Array.replaceMap(
			self.lines, self.cursor.lineNo,
			function(line)
				return line:delete(self.cursor.column, 1)
			end
		)

		return TextEditor.new(newLines, newCursor)
	else
		if self.cursor.lineNo > 1 then
			local prevLine = self.lines[self.cursor.lineNo - 1]
			local newLines = Array.replaceMap(self.lines, self.cursor.lineNo, function(line)
				return Line.new(prevLine.text .. line.text)
			end)
			local newLines = Array.remove(newLines, self.cursor.lineNo - 1)

			local newCursor = Cursor.new(self.cursor.lineNo - 1, #prevLine.text)

			return TextEditor.new(newLines, newCursor)
		else
			return self
		end
	end
end

return TextEditor
