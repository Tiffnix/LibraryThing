local Line = {}
Line.__index = Line

function Line.new(text)
	local self = {
		text = text,
	}
	setmetatable(self, Line)

	return self
end

function Line.__eq(left, right)
	return
		left.text == right.text
end

function Line:insert(str, position)
	if position < 0 then
		position = #self.text + 1 + position
	end

	local newText =
		self.text:sub(1, position) ..
		str ..
		self.text:sub(position + 1, #self.text)

	return Line.new(newText)
end

function Line:delete(position, length)
	if position < 0 then
		position = #self.text + 1 + position
	end

	local newText =
		self.text:sub(1, position - 1) ..
		self.text:sub(position + length, -1)

	return Line.new(newText)
end

return Line
