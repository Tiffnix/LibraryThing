return function()
	local Array = require(script.Parent.Array)

	describe("Array.insert", function()
		it("should allow inserting at 0", function()
			local example = { "bar", "baz" }
			local result = Array.insert(example, 0, "foo")
			expect(result[1]).to.equal("foo")
			expect(result[2]).to.equal("bar")
			expect(result[3]).to.equal("baz")
			expect(#result).to.equal(3)
		end)

		it("should allow inserting at 1", function()
			local example = { "foo", "baz" }
			local result = Array.insert(example, 1, "bar")
			expect(result[1]).to.equal("foo")
			expect(result[2]).to.equal("bar")
			expect(result[3]).to.equal("baz")
			expect(#result).to.equal(3)
		end)

	end)
end
